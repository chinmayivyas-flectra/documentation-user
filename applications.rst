============
Applications
============

.. toctree::
   :titlesonly:

   accounting
   crm
   sales
   website
   ecommerce
   discuss
   purchase
   inventory
   manufacturing
   point_of_sale
   project
   helpdesk
   livechat/livechat
   expense/expense
   l10n_in_gst/gst
   general
..   expenses
..   recruitment
